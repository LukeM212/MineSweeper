const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const cellSize = 25;
const width = 16;
const height = 16;
const bombs = 50;

var grid;

//	States (flags):
//		0 - empty
//		1 - bomb
//		2 - flaggedEmpty
//		3 - flaggedBomb
//		4 - opened

function discover(x, y) {
	grid[x][y] = 4;
	ctx.fillStyle = "#FFF";
	ctx.fillRect(x * cellSize + 1, y * cellSize + 1, cellSize - 1, cellSize - 1);
	
	var bombCount = 0;
	for (var dx = -1; dx <= 1; dx++)
		for (var dy = -1; dy <= 1; dy++)
			if (x + dx >= 0 && x + dx < width && y + dy >= 0 && y + dy < width && grid[x + dx][y + dy] % 2 == 1)
				bombCount++;
	
	if (bombCount == 0) {
		for (var dx = -1; dx <= 1; dx++)
			for (var dy = -1; dy <= 1; dy++)
				if (x + dx >= 0 && x + dx < width && y + dy >= 0 && y + dy < width && grid[x + dx][y + dy] != 4)
					discover(x + dx, y + dy);
	} else {
		ctx.fillStyle = "#000";
		ctx.textAlign = "center";
		ctx.fillText(bombCount, (x + 0.5) * cellSize, (y + 0.5) * cellSize);
	}
}

canvas.onmousedown = function(e) {
	var x = Math.floor((e.pageX - canvas.offsetLeft) / cellSize);
	var y = Math.floor((e.pageY - canvas.offsetTop) / cellSize);
	
	if (x >= width || y >= height) return;
	
	if (e.button == 0) {
		switch (grid[x][y]) {
			case 0:
				discover(x, y);
				break;
			case 1:
				ctx.fillStyle = "#000";
				ctx.beginPath();
				ctx.ellipse((x + 0.5) * cellSize, (y + 0.5) * cellSize, cellSize / 4, cellSize / 4, 0, 0, Math.PI * 2);
				ctx.fill();
				alert("YOU LOSE!!!");
				break;
		}
	} else if (e.button == 2) {
		switch (grid[x][y]) {
			case 0: case 1:
				grid[x][y] += 2;
				ctx.fillStyle = "#A00";
				ctx.beginPath();
				ctx.ellipse((x + 0.5) * cellSize, (y + 0.5) * cellSize, cellSize / 4, cellSize / 4, 0, 0, Math.PI * 2);
				ctx.fill();
				break;
			case 2: case 3:
				grid[x][y] -= 2;
				ctx.fillStyle = "#DDD";
				ctx.fillRect(x * cellSize + 1, y * cellSize + 1, cellSize - 1, cellSize - 1);
				break;
		}
	}
	
	return false;
}

canvas.oncontextmenu = e => false;

function setup() {
	canvas.width = width * cellSize + 1;
	canvas.height = height * cellSize + 1;
	
	ctx.fillStyle = "#000";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	
	ctx.fillStyle = "#DDD";
	grid = [];
	for (var x = 0; x < width; x++) {
		grid[x] = [];
		for (var y = 0; y < height; y++) {
			grid[x][y] = 0;
			ctx.fillRect(x * cellSize + 1, y * cellSize + 1, cellSize - 1, cellSize - 1);
		}
	}
	
	var bombsLeft = bombs;
	while (bombsLeft > 0) {
		var x = Math.floor(Math.random() * width);
		var y = Math.floor(Math.random() * height);
		
		if (grid[x][y] == 0) {
			grid[x][y] = 1;
			bombsLeft--;
		}
	}
}

setup();
